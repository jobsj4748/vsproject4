﻿#include <iostream>
#include <time.h>
#include <iomanip>
#include <Windows.h>

using std::cout;

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int Sum = 0;
	const int Size = 5;
	int Modulo = buf.tm_mday % Size;

	int array[Size][Size];

	int Line;

	for (int i = 0; i < Size; i++)
	{
		cout << "Line_" << i << ":  ";

		for (int j = 0; j < Size; j++)
		{
			array[i][j] = i + j;

			cout << array[i][j] << ' ';

			if (Modulo == i)
			{
				Line = i;

				Sum += array[i][j];
			}
		}
		cout << "\n";
	}

	cout << "--------\n" << "\nDay: " << Modulo << "\nCurrent Line: " << Line << "\nSum: " << Sum << '\n';
	}